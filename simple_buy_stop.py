from ibapi.client import EClient
from ibapi.ticktype import TickTypeEnum
from ibapi.wrapper import EWrapper
import threading
import time
import random
from ibapi.contract import Contract
from datetime import datetime
from tick_data_connect import check_connection
from buy_stop_exec import *

info = {"stop": False}


class IBapi(EWrapper, EClient):

    def __init__(self):
        EClient.__init__(self, self)
        self.order_count = 0
        self.loop_count = 0
        self.price_list = []
        self.min_tick = 0
        self.last_fill_price = 0
        self.start_time = 0
        self.wait_time = 1000

    def tickPrice(self, reqId, tickType, price, attrib):
        if self.loop_count == 0:
            check_connection(price)

        if self.loop_count > 1:
            if tickType == TickTypeEnum.BID and reqId == 1:
                self.price_list.append(price)
                # print(price)

                # Check if number of orders placed equals 2
                if self.order_count == 2:
                    # set this value to true to stop loop and exit program
                    info["stop"] = True

                if self.order_count == 1:
                    time_diff = datetime.now() - self.start_time

                    if price >= (self.last_fill_price + (8 * self.min_tick)) and self.last_fill_price != 0:
                        print("price = ", price)
                        print("Last fill price = ", self.last_fill_price)
                        stop_price = (self.last_fill_price + (4 * self.min_tick))
                        order_thread_function(es_contract, "SELL", "STP", 1, app, stop_price)
                    elif time_diff.total_seconds() >= self.wait_time:
                        order_thread_function(es_contract, "SELL", "MKT", 1, app)
                    else:
                        print("Elapsed time = ", time_diff.total_seconds())

        self.loop_count += 1

    def nextValidId(self, orderId: int):
        super().nextValidId(orderId)
        self.nextorderId = orderId
        # print('The next valid order id is: ', self.nextorderId)

    def orderStatus(self, orderId, status, filled, remaining, avgFullPrice, permId, parentId, lastFillPrice, clientId,
                    whyHeld, mktCapPrice):
        print('orderStatus - orderid:', orderId, 'status:', status, 'filled', filled, 'remaining', remaining,
              'lastFillPrice', lastFillPrice)
        self.last_fill_price = lastFillPrice

    def openOrder(self, orderId, contract, order, orderState):
        print('openOrder id:', orderId, contract.symbol, contract.secType, '@', contract.exchange, ':', order.action,
              order.orderType, order.totalQuantity, orderState.status)

    def execDetails(self, reqId, contract, execution):
        print('Order Executed: ', reqId, contract.symbol, contract.secType, contract.currency, execution.execId,
              execution.orderId, execution.shares, execution.lastLiquidity)

    def contractDetails(self, reqId, contractDetails):
        # print(contractDetails.minTick)
        self.min_tick = contractDetails.minTick

    def contractDetailsEnd(self, reqId):
        print("Contract details found! - ", reqId)


def run_loop():
    app.run()


app = IBapi()
app.connect('127.0.0.1', 7497, random.randint(1, 50000))

# Set nextOrderId to one
app.nextorderId = 1

# Start the socket in a thread
api_thread = threading.Thread(target=run_loop, daemon=True)
api_thread.start()


def create_contract(symbol, sec_type, exchange, trade_date):
    # Create contract object
    contract = Contract()
    contract.symbol = symbol
    contract.secType = sec_type
    contract.exchange = exchange
    contract.currency = 'USD'
    contract.lastTradeDateOrContractMonth = trade_date

    return contract


es_contract = create_contract('ES', 'FUT', 'GLOBEX', "20210917")

# Request contract details
app.reqContractDetails(1004, es_contract)

# Request Market Data
app.reqMktData(1, es_contract, '', False, False, [])
time.sleep(2)


# setup new thread to place an order
def order_thread_function(contract, action, order_type, quantity, app, stop_price=0):
    new_order_thread = threading.Thread(target=make_order,
                                        args=(contract, action, order_type, quantity, app, stop_price),
                                        daemon=True)
    new_order_thread.start()
    app.order_count += 1


user_input = input("Press enter to trigger market buy order")
if user_input == "":
    # Call the order_thread_function to place an order and start the order thread
    order_thread_function(es_contract, "BUY", "MKT", 1, app)
    # Set start_time to current time
    app.start_time = datetime.now()

while not info["stop"]:
    time.sleep(1)  # Sleep interval to allow time for incoming price data

app.disconnect()
