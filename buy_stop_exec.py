from ibapi.tag_value import TagValue
from ibapi.order import *


# Custom function to create and place an order
def make_order(contract, action, order_type, quantity, app, stop_price=0):
    # Create new order
    order = Order()
    order.action = action
    order.totalQuantity = quantity
    order.orderType = order_type
    # Conditions to check if a stop order is being placed
    if stop_price == 0:
        order.algoStrategy = 'Adaptive'
        order.algoParams = []
        order.algoParams.append(TagValue("adaptivePriority", "Patient"))
    elif order_type == "STP" and stop_price != 0:
        order.auxPrice = stop_price

    # This line places an order
    app.placeOrder(app.nextorderId, contract, order)
    # Increment nextorderId by 1
    app.nextorderId += 1
