import time


# Check if API is connected
def check_connection(price):
    while True:
        if price > 1:
            print("connected\n")
            break
        else:
            print("waiting for connection...")
            time.sleep(1)  # Sleep interval to allow time for connection to server
    return
